DROP TABLE IF EXISTS users;
CREATE TABLE users (
  id SERIAL,
  name text,
  surname text,
  email TEXT UNIQUE,
  date_added timestamp default now()
);
