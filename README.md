# User data import #

### Description ###

- Main script is user_upload.php, a single executable file from CLI only
- Imports users data (name, surname and email) into users db table using --file as the argument
- File argument needs to be a CSV file residing in the same directory as the user_upload.php
- Uses database connection parameters as arguments
- Also uses verbose output, re-creating users db, dry run (without db insert) and help as other parameters

### Example commands ###

1. php user_upload.php -h "localhost" -d "test_dc" -u "postgres" -p "password" --create_table --verbose
Connects to Postgres database and creates users db with id, name, surname, email and date_added as columns

2. php user_upload.php --file "users.csv" -h "localhost" -d "test_dc" -u "postgres" -p "password" --verbose
Connects to Postgres database and populates users data from users.csv file after validating the email

3. php user_upload.php --file "users.csv" -h "localhost" -d "test_dc" -u "postgres" -p "password" --verbose --dry_run
Connects to Postgres database and validates users data from users.csv file without inserting them into users db table

4. php user_upload.php --help
Provides help on how to use the script and its command line arguments arguments

### prerequisite ###

- PHP 7.2+
- Postgres database 9.5+