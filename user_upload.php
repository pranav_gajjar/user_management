<?php

/**
 * Author: Pranav Gajjar <pranavgajjar@gmail.com>
 * Date: 13 July 2020
 * 
 * Script runs on CLI to process a list of users from a CSV file
 * validates them and inserts them into a postgres database table of users
 */

# setting up the acceptable short and long CLI arguments
# db username, db password, db name and db hostname are short arguments
$shortopts = "u:";
$shortopts .= "p:";
$shortopts .= "h:";
$shortopts .= "d:";

# CSV file, creating a user table, dry run without a db manipulation and help arguments are long arguments
$longopts  = array(
    "file:",
    "create_table",
    "dry_run",
    "help",
    "verbose"
);
$options = getopt($shortopts, $longopts);
if(empty($options)) {
    printError("Please provide one or more arguments. Please use --help to get help in executing the script");
}

# initializing CLI argument variables
$dbuser = $dbpass = $dbhost = $dbname = $file = $verbose = null;
$create_table = $dry_run = $help = false;

# SQL file that contains users table schema
$create_users_table_file = "create_users_table.sql";

# assigning appropriate values to CLI variables
foreach ($options as $opt_key => $opt_val) {
	switch ($opt_key) {
        case 'u':
            $dbuser = $opt_val;
            break;
        case 'p':
            $dbpass = $opt_val;
            break;
        case 'd':
            $dbname = $opt_val;
            break;
        case 'h':
            $dbhost = $opt_val;
            break;
        case 'file':
            $file = $opt_val;
            break;
        case 'create_table':
            $create_table = true;
            break;
        case 'dry_run':
            $dry_run = true;
            break;
        case 'verbose':
            $verbose = true;
            break;
        case 'help':
            $help = true;
            break;
    }
}

# printing help for the execution of script on STDOUT
if($help) {
    print "\n\nScript runs on CLI to process a list of users from a CSV file, validates them and inserts them into a postgres database table of users\n\n";
    print "Following arguments are acceptable\n
    -u Database username
    -p Database password
    -h Database hostname
    -d Database name
    --file CSV file name which contains a list of users with columns as name, surname and email
    --create_table To let script know to create the users db table
    --dry_run Execute the script without any DB insertion
    --verbose To print the debug output while script execution
    --help To get this help\n
    ";
    exit;
}

printOut("Script started");
printOut("--------------\n");
printOut("CLI arguments: dbuser:{$dbuser}, dbpass:{$dbpass}, dbhost:{$dbhost}, dbname:{$dbname}, file:{$file}, verbose:{$verbose}, create_table:{$create_table}, dry_run:{$dry_run}, help:{$help}\n");

# connecting to the postgres database using the connection information passed through CLI arguments
$db_con_str = "host={$dbhost} dbname={$dbname} user={$dbuser} password={$dbpass}";
$db_con = @pg_pconnect($db_con_str) or die("Couldn't connect to the postgres database using: ".$db_con_str."\n\n");

# checking if the users db table schema file exists in the current directory
# if we were asked to re-create that db table
if($create_table) {
    if(!is_file(getcwd()."/".$create_users_table_file)) {
        printError("The file: ".$create_users_table_file." doesn't exist! Please make sure it exists in the same directory as user_upload.php\n");
    }

    # re-creating the users db table if asked
    $create_users_table = file_get_contents(getcwd()."/".$create_users_table_file);
    $result = @pg_exec($db_con, $create_users_table);
    if($result === false) {
        printError(pg_last_error());
    }

    printOut("db table: users created");
    printOut("Script completed",true);
}

# check if the data CSV file provided exists in the current directory
if(!isset($file) || !is_file(getcwd()."/".$file)) {
    printError("The file: ".$file." doesn't exist or not provided! Please make sure it exists in the same directory as user_upload.php\n");
}

# populating the users data from the CSV file
$csv_handle = fopen(getcwd()."/".$file, "r");
$counter = $inserted = $not_inserted = 0;
while($line = fgets($csv_handle)) {
    $counter++;
    
    # ignoring the first line
    if($counter == 1) continue;

    # checking if the input provided
    if(empty($line)) {
        continue;
    }

    # removing line endings
    $line = str_replace(array("\r\n", "\r", "\n"), "", $line);
    
    # collecting name, surname and email from the line
    list($name, $surname, $email) = explode(",", $line);

    # removing extra spaces and lowering the case
    cleanUpInput($name);
    cleanUpInput($surname);
    cleanUpInput($email);

    # checking if there is a valid email address given
    if(!validate_email($email)) {
        printError("Row:".$counter.", please specify a valid email address (".$email.")", false);
        $not_inserted++;
        continue;
    }
    
    # checking if the user email already exists
    $email_record = is_email_duplicate($email);
    if($email_record) {
        printError("Row:".$counter.", email address already exists (".$email.")", false);
        $not_inserted++;
        continue;
    }

    # inserting a user's record if all look good
    $result = insert_user(ucfirst($name), ucfirst($surname), $email);
    if($result) {
        $inserted++;
    }
}

printOut("Script execution summary");
printOut("Rows processed: {$counter}, total inserted: {$inserted}, total not inserted: {$not_inserted}");
printOut("Script completed",true);

###########################################
############# Functions ###################

/**
 * removes trailing spaces and lowers the string
 */
function cleanUpInput(&$str) {
    $str = strtolower(trim($str));
}

/**
 * prints a customized message to STDOUT
 */
function printError($str, $exit = true) {
    print date("Y-m-d H:i:s")."\t".$str."\n";
    if($exit)
    exit(0);
}

/**
 * prints the output on STDOUT if the verbose is set
 */
function printOut($str, $exit_out = false) {
    global $verbose;

    if($verbose)
        print date("Y-m-d H:i:s")."\t".$str."\n";

    if($exit_out)
        exit(0);
}

/**
 * Validating email address. 
 */
function validate_email($email) {
    return (boolean) filter_var($email, FILTER_VALIDATE_EMAIL);
}

/**
 * checking if the user email already exists in the users db table
 */
function is_email_duplicate($email) {
    global $db_con;
    $query = "SELECT id from users where email = '".pg_escape_string($email)."'";
    $result = @pg_query($db_con, $query);
    if($result === false) {
        printError(pg_last_error());
    }
    else if($row = pg_fetch_row($result)) {
        return $row['0'];
    }
    return false;
}

/**
 * inserting a user row into the users database table
 * db insertion will be skipped if dry run is specified
 */
function insert_user($name, $surname, $email) {
    global $db_con, $dry_run;

    # incase of dry run, do not execute the SQL
    if($dry_run)
    return true;

    $query = "INSERT INTO users(name, surname, email) VALUES ('".pg_escape_string($name)."', '".pg_escape_string($surname)."', '".pg_escape_string($email)."')";
    $result = @pg_query($db_con, $query);
    if($result === false) {
        printError(pg_last_error());
    }
    return $result;
}

?>